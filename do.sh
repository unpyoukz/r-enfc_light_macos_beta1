source $HOME/.bash_profile

export PYTHONPATH=$PWD:$LEGION_DIR/bindings/python

make -j
./main2.exec -ll:cpu 8 -ll:csize 6000 -ll:io 1 -ll:util 1 -ll:bgwork 1 -ll:py 1 -ll:pyimport python_main
