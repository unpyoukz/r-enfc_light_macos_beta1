#Required paths
ifndef LEGION_DIR
  $(error LEGION_DIR is not set)
endif

DYNLINK_PATH := DYLD_LIBRARY_PATH:DYLD_FALLBACK_LIBRARY_PATH

# CUDA options
USE_CUDA ?= 1

# HDF options
#export USE_HDF ?= 1
#export HDF_HEADER ?= hdf5.h
export HDF_HEADER=$(LEGION_DIR)/language/hdf/install/include/hdf5.h
#export HDF_HEADER=$(HDF_ROOT)/include/hdf5.h
HDF_LIBNAME ?= hdf5

# C compiler options
CFLAGS += -O2 -Wall -Werror -fno-strict-aliasing -I$(LEGION_DIR)/runtime -DREALM_MAX_DIM=4 -DLEGION_MAX_DIM=4
CXXFLAGS += -std=c++11 -O3 -Wall -Werror -fno-strict-aliasing -I$(LEGION_DIR)/runtime -DREALM_MAX_DIM=4 -DLEGION_MAX_DIM=4

# Regent options
REGENT := $(LEGION_DIR)/language/regent.py
#ifeq ($(USE_CUDA), 1)
#  REGENT_FLAGS := -fflow 0 -fopenmp 1 -foverride-demand-openmp 1 -finner 1 -fcuda 1 -fcuda-offline 1 -foverride-demand-cuda 1 -foverride-demand-index-launch 1 -fpretty 1
#else
#  REGENT_FLAGS := -fflow 0 -fopenmp 1 -foverride-demand-openmp 1 -finner 1 -fcuda 0
REGENT_FLAGS := -fflow 0 -fopenmp 0 -foverride-demand-openmp 1 -finner 1 -fcuda 0 -fpretty 0
#endif

# Link flags
LINK_FLAGS += -L$(LEGION_DIR)/bindings/regent -lregent
ifdef HDF_ROOT
  LINK_FLAGS += -L$(HDF_ROOT)/lib
endif
ifeq ($(USE_HDF), 1)
  LINK_FLAGS += -l$(HDF_LIBNAME)
endif
LINK_FLAGS += -lm
 
ifeq ($(DEBUG), 1)
  REGENT_FLAGS += -g
  CFLAGS += -g
  CXXFLAGS += -g
  LINK_FLAGS += -g
endif

.PHONY: default all clean
.PRECIOUS: main2_%.o

default: main2.exec

main2.exec: main2.o
	 $(CXX) -o $@ $^ $(LINK_FLAGS)

main2.o: main2.rg
	 $(REGENT) main2.rg $(REGENT_FLAGS)

clean:
	 $(RM) *.exec *.o
