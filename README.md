# R-EnFC_light_macOS_beta1 (Regent-based Ensemble Flow Code light version for macOS beta1)
Oct 4, 2022
Kazuki Maeda (kemaeda@stanford.edu)

R-EnFC_light_macOS_beta1 is a light version of R-EnFC which works on macOS. R-EnFC is a code developed for reserch purposes to demonstrate Legion-based ensemble simulations of complex flows using Regent-API and Python modules for coupled ensemble simulations and in-situ data processing. 


Citation (Dec 31, 2022): Kazuki Maeda and Thiago Teixeira (2022).
          Task-based framework for physics-based ensemble simulation and in-situ data processing.
          Annual Research Briefs, Center for Turbulence Research, Stanford University, pp 97–110.

## Organization
./
   > [do.sh](do.sh): Execution script

   > [Makefile](Makefile): Makefile 

   > [main2.rg](main2.rg): Regent source code containing main tasks 

   > [python_main.py](python_main.py): Python source code containing Pygion tasks exported from Regent 


## Installation
Install homebrew following, for instance, the webpage below.

```
https://www.groovypost.com/howto/install-homebrew-macos/
```

Open Terminal and go to your home directory.

```
cd $HOME
```

Install git using homebrew.
```
brew install git
```

Edit `.bash_profile` in your HOME (create one if you do not have it) as follows. Make sure to use your name for `[your username]` as appropriate.

```
export CLICOLOR=1
  
# Build config
export INCLUDE_PATH="$(xcrun --sdk macosx --show-sdk-path)/usr/include:$INCLUDE_PATH"
export CXX_FLAGS="-std=c++11"
   
# Path setup
export LEGION_DIR=/Users/[your usename]/Legion
export HDF_ROOT="$LEGION_DIR"/language/hdf/install
 
export CC=gcc
export CXX=g++

# Legion setup
export DYLD_LIBRARY_PATH=$LEGION_DIR/bindings/regent:$LEGION_DIR/language/hdf/install/lib

export USE_CUDA=0
export USE_OPENMP=0
export USE_GASNET=0
export USE_HDF=1
export MAX_DIM=3

export REGENT=$LEGION_DIR/language/regent.py
```

(To enable OPENMP, for example for multithreading of Regent and Python tasks, one may use OpenMP and install OpenBLAS prior to installing Legion.)

To use legion_python in mac, further add the following lines.
```
USE_PYTHON=1
alias python=/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.9/bin/python3
alias python3=/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.9/bin/python3
export PYTHONPATH=/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.9/bin:"$LEGION_DIR"/bindings/python
export PYTHON_LIB=/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.9/lib/libpython3.9.dylib
export PYTHON_VERSION_MAJOR=3

export lpy="$LEGION_DIR"/bindings/python/legion_python
```

Source `.bash_profile`

```
source .bash_profile
```

Clone Legion into `$LEGION_DIR`

```
git clone -b control_replication https://gitlab.com/StanfordLegion/legion.git "$LEGION_DIR"
```

Go to the Legion directory.

```
cd $LEGION_DIR
```

Pull the following commit as follows.

```
git reset --hard 185c565d0c12356d4723b22bff808b5f84768cc5
```

Manually install hdf5.

```
cd $LEGION_DIR/langauge
mkdir $LEGION_DIR/language/hdf
cd $LEGION_DIR/language/hdf
wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=1QnaDd7pnoS_Io9__vNgp2xhZqNUxh9g4' -O hdf-1.10.8.zip
unzip hdf5-1.10.8.zip
cd hdf5-1.10.8
./configure --prefix=$HDF_ROOT --enable-threadsafe --disable-hl
make install
```

Change the name of hdf5's dynamic library referred in `$LEGION_DIR/language/scripts/setup_env.py` from `libhdf5.so` to `libhdf5.dynlib` as below (line numbers may differ).
```
(in setup_env.py)
520 #        hdf_build_result = os.path.join(hdf_install_dir, 'lib', 'libhdf5.so')
521         hdf_build_result = os.path.join(hdf_install_dir, 'lib', 'libhdf5.dylib')
```
(line numbers may differ depending on the version of Legion)
Install Legion. This may take a while.

```
cd $LEGION_DIR/langauge
./scripts/setup_env.py
```

You may have to manually install other softwares such as cmake through homebrew (check out error messages from the Legion installation).


### Build R-EnFC
```
cd [Your R-EnFC directory]
make -j
```

### Running R-EnFC
```
do.sh
```
