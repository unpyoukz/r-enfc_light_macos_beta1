-- Copyright (c) "2022, by Stanford University
--               Developer: Kazuki Maeda
--               Affiliation: Center for Turbulence Research, Stanford University
--               Citation: Kazuki Maeda and Thiago Teixeira (2022).
--                         Task-based framework for physics-based ensemble simulation and in-situ data processing.
--                         Annual Research Briefs, Center for Turbulence Research, Stanford University, pp 97–110.

-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--    * Redistributions of source code must retain the above copyright
--      notice, this list of conditions and the following disclaimer.
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import "regent"

-------------------------------
------ CHANGE ONLY HERE -------
-------------------------------
-- Logistics
local ADV  = true -- turn on Advection
local HDF   = false -- HDF output
local PY    = true-- call Pygion
local NE   = 4  -- number of the ensemble members
local DIMN  = 2 -- spatial dimension
local RKN   = 2 -- RK order (1 or 3 by default. if using RKN=2, comment in RK for RK2 below)

-- Grid cells in each sample (all samples share the same values)
-- Domain size
local Lx = 0.1   -- m
local Ly = 0.05  -- m
local Lz = 0.05

-- x and y resolution
local nx = 80
local ny = 40
local nz = 2

local H_jet = 0.1 -- Characteristic jet thickness (unit: Ly)
local Jet_vel = 200 -- Jet velocity (m/s)

-- Config
local conf = {}
conf.dt    = 0.000001 -- time step size (s)
conf.step  = 1000 -- total time step
conf.save  = 320 -- save frequency (in time step)
conf.py    = 320 -- python data transfer frequency (in time step)

-------------------------------
----- DO NOT CHANGE BELOW -----
-------------------------------

conf.t1    = conf.dt*(conf.step+1) -- end time 
conf.debug = 10  -- conf.step

local DEBUG = false
local BCx   = 2 
local BCy   = 1 
local BCz   = 1
local CASE =  6

-- Math
local fabs   = regentlib.fabs(double)
local sqrt   = regentlib.sqrt(double)
local exp    = regentlib.exp(double)
local copysign = regentlib.copysign(double)
local tanh   = regentlib.tanh(double)
local sin    = regentlib.sin(double)
local cos    = regentlib.cos(double)
local floor  = regentlib.floor(double)
local pow    = regentlib.pow(double)
local Pi     = 3.141592653589793

-- Import
local C = regentlib.c
local sym = regentlib.newsymbol
local gett = C.legion_get_current_time_in_micros

-- Util
local format = require("std/format")

-- Number of buffer cells at the edges of each tile
local bf = 2

-- Time stepping scheme coefficients
local RK
-- RK2
RK = {
[1] = {1.0, 0.0, 1.0},
[2] = {1.0/2.0, 1.0/2.0, 1.0/2.0},
[3] = {0.0, 0.0, 0.0},
}

-- Time stepping scheme coefficients
local RKT = {1.0, 1.0/4.0, 2.0/3.0}

-- Parameters (all physical units are in SI)
local eps = 10^(-16)
local weps = 10^(-40)
local Mu = 0.000018--0.11832--1.0 -- Viscosity

-- Outlet pressure
local P_out = 101325.0

-- Inlet parameters
local P_in = 101325.0*1.5
local R_in = 1.2
local U_in = 430.0

-- Reference P and rho
local P_ref = 101325.0
local R_ref = 1.0

-- Molecular weight of fluid 1 and fuid 2
local W1 = 32*1.0e-3
local W2 = 16*1.0e-3

-- Specific heat ratio of fluid 1 and fluid 2 (given as constant)
local Gam1 = 1.4
local Gam2 = 1.32
local Gami_1 = 1.0/(Gam1-1.0)
local Gami_2 = 1.0/(Gam2-1.0)

-- Field variables
struct fc {
   x : double;
   y : double;
   z : double;
   dx : double;
   dy : double;
   dz : double;
   r : double;
   r2 : double;
   u : double;
   v : double;
   w : double;
   p : double;
   mu : double;
   mv : double;
   mw : double;
   e : double;
   dr : double;
   dr2 : double;
   dmu : double;
   dmv : double;
   dmw : double;
   de : double;
   gf : double;
   rc: double;
   r2c: double;
   muc: double;
   mvc: double;
   mwc: double;
   ec: double;
}

struct fc2 {
   r : double;
   r2 : double;
   mu : double;
   mv : double;
   mw : double;
   e : double;
}

-------------
--- Tasks ---
-------------

-- Initialize grid
--__demand(__leaf,__cuda)
task Grid(ff: region(ispace(int3d), fc))
where
    writes(ff.{x,y,z,dx,dy,dz,gf})
do
     __demand(__openmp)
    for c in ff do
        ff[c].dx = Lx/double(nx)
        ff[c].x  = Lx/double(nx)*double(c.x)-Lx/2.0 - Lx/double(nx)*(bf-0.5)
        ff[c].dy = Ly/double(ny)
        ff[c].y  = Ly/double(ny)*double(c.y)-Ly/2.0 - Ly/double(ny)*(bf-0.5)
        ff[c].dz = Lz/double(nz)
        ff[c].z  = Lz/double(nz)*double(c.z)-Lz/2.0 - Lz/double(nz)*(bf-0.5)

        for bn = 1, bf+1 do
           if ( (c.x-bn+1)*(nx+2*bf-(c.x+bn)) == 0) then
               ff[c].gf = 1.0
           end
        end

        for bn = 1, bf+1 do
           if ( (c.y-bn+1)*(ny+2*bf-(c.y+bn)) == 0) then
               ff[c].gf = 1.0
           end
        end

        for bn = 1, bf+1 do
           if ( (c.z-bn+1)*(nz+2*bf-(c.z+bn)) == 0) then
               ff[c].gf = 1.0
           end
        end
         
    end
end


-- Initialize field
--__demand(__leaf,__cuda)
task Init(ff: region(ispace(int3d), fc), id:int32)
where
    reads (ff.{x,y,z}),
    reads writes(ff.{r,r2,mu,mv,mw,e,p})
do
--     __demand(__openmp)
    for c in ff do

       var p: double
       var r: double
       var r2: double
       var u: double
       var v: double
       var w: double
       var x = ff[c].x
       var y = ff[c].y
       var z = ff[c].z

rescape if CASE == 0 then print('||||| Initialize 1D density wave advection |||||') remit rquote

       p = 101325.0
       u = 100.0

       v = 0.0; w = 0.0
       r = 1.0+2.0*exp(-(x*x)/0.01) -- 1d
 
       ff[c].r = r
       ff[c].mu = r*u
       ff[c].mv = r*v
       ff[c].mw = r*w
       ff[c].e = p/(Gam-1.0) + (u*u+v*v+w*w)*r/2.0

end end end


rescape if CASE == 1 then print('||||| Initialize Sod shocktube |||||') remit rquote

-- Sod
--      p = 101325.0
--      u = 0.0
--      v = 0.0
--      w = 0.0
--      --r = 1.0+5.0*exp(-(x*x+y*y+z*z)/0.01)
--      --r = 1.0+5.0*exp(-((x-0.25)*(x-0.25)+y*y)/0.01)
--      r = 1.0
--      ff[c].r = r
--      --ff[c].p = 10.0*p
--      ff[c].mu = r*u
--      ff[c].mv = r*v
--      ff[c].mw = r*w
--      ff[c].e = p/(Gam-1.0)+ r*(u*u+v*v+w*w)/2.0
--
--       if x < 0.0 then
--          p = 10*101325.0
--          u = 0.0
--          v = 0.0
--          w = 0.0
--          ff[c].r = 8.0
--          --ff[c].p = 10.0*p
--          ff[c].mu = ff[c].r*u
--          ff[c].mv = ff[c].r*v
--          ff[c].mw = ff[c].r*w
--          ff[c].e = p/(Gam-1.0)+ (ff[c].mu*ff[c].mu+ff[c].mv*ff[c].mv+ff[c].mw*ff[c].mw)/2.0/ff[c].r
--       end
--

-- Lax
      p = 101325.0
      u = 0.0
      v = 0.0
      w = 0.0
      --r = 1.0+5.0*exp(-(x*x+y*y+z*z)/0.01)
      --r = 1.0+5.0*exp(-((x-0.25)*(x-0.25)+y*y)/0.01)
      r = 1.0
      r2 = eps
      ff[c].r = 0.5
      --ff[c].p = 10.0*p
      ff[c].mu = 0.0
      ff[c].mv = (r+r2)*v
      ff[c].mw = (r+r2)*w
      ff[c].e = 1.4275--p/(Gam-1.0) + r*(u*u+v*v+w*w)/2.0

       if x < 0.0 then
          p = 10*101325.0
          u = 0.0
          v = 0.0
          w = 0.0
          ff[c].r = 0.445--8.0
          ff[c].r2 = eps
          --ff[c].p = 10.0*p
          ff[c].mu = 0.311--ff[c].r*u
          ff[c].mv = (ff[c].r+ff[c].r2)*v
          ff[c].mw = (ff[c].r+ff[c].r2)*w
          ff[c].e = 8.928--p/(Gam-1.0)+ (ff[c].mu*ff[c].mu+ff[c].mv*ff[c].mv+ff[c].mw*ff[c].mw)/2.0/ff[c].r
       end
--

end end end


rescape if CASE == 2 then print('||||| Initialize shear layer |||||') remit rquote

-- Shear layer
      x = ff[c].x
      y = ff[c].y
      var rand = sin(2.0*Pi*x+0.1) + sin(2.0*2.0*Pi*x+0.23) + sin(4.0*2.0*Pi*x+0.79)
      p = 101325.0*(1.0+0.00001*rand*exp(-y*y/0.001))
      --u = 10.0*tanh(40.0*ff[c].y*(1.0+0.0001*rand*exp(-y*y/0.001)))
      u = 50.0*tanh(40.0*ff[c].y)
      v = 0.0
      w = 0.0
      r = 1.0
      r2 = 0.5
      ff[c].r = r
      ff[c].r2 = r2
      ff[c].mu = (r+r2)*u
      ff[c].mv = (r+r2)*v
      ff[c].mw = (r+r2)*w

      var a1 = r*W2/(r*W2+r2*W1)
      var a2 = 1.0-a1
      var gami = a1*Gami_1+a2*Gami_2
      ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0

end end end


rescape if CASE == 3 then print('||||| Initialize Acoustic pulse |||||') remit rquote

-- Pulse
      p = 101325.0+10.0*exp(-(x*x+y*y+z*z)/(0.02*0.02)) -- 3d
    --p = 101325.0+10.0*exp(-(x*x+y*y)/(0.02*0.02)) -- 2d
    --p = 101325.0+10.0*exp(-(x*x)/(0.02*0.02)) -- 1d
      u = 0.0
      v = 0.0
      w = 0.0
      r = 1.0
      ff[c].r = r
      ff[c].r2 = eps
      ff[c].mu = (r+r2)*u
      ff[c].mv = (r+r2)*v
      ff[c].mw = (r+r2)*w
      var a1 = r*W2/(r*W2+r2*W1)
      var a2 = 1.0-a1
      var gami = a1*Gami_1+a2*Gami_2
      ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0

end end end


rescape if CASE == 4 then print('||||| Initialize 3D density wave advection |||||') remit rquote

      p = 101325.0
      u = 400.0; v = 400.0; w = 400.0
      r = 1.0+2.0*exp(-(x*x+y*y+z*z)/0.01) -- 3d
      r2 = 1.0
      v = 0.0
      w = 0.0

      ff[c].r = r
      ff[c].r2 = r2
      ff[c].mu = (r+r2)*u
      ff[c].mv = (r+r2)*v
      ff[c].mw = (r+r2)*w

      var a1 = r*W2/(r*W2+r2*W1)
      var a2 = 1.0-a1
      var gami = a1*Gami_1+a2*Gami_2
      ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0

end end end


rescape if CASE == 5 then print('||||| Initialize 3D TGV |||||') remit rquote

      var phi = 0.0
      var u0 = 11.83216
      var p0 = 10000
      p = p0+0.25*0.25*u0*u0*(cos(2*(x-phi))+cos(2*(y-phi)))*(cos(2*(z-phi))+2.0)
      u = u0*sin(x-phi)*cos(y-phi)*cos(z-phi)
      v = -u0*cos(x-phi)*sin(y-phi)*cos(z-phi)
      w = 0.0
      r = 1+0.25*0.25*u0*u0*(cos(2*(x-phi))+cos(2*(y-phi)))*(cos(2*(z-phi))+2.0)/p0

      ff[c].r = r
      ff[c].r2 = eps
      ff[c].mu = (r+r2)*u
      ff[c].mv = (r+r2)*v
      ff[c].mw = (r+r2)*w
      var a1 = r*W2/(r*W2+r2*W1)
      var a2 = 1.0-a1
      var gami = a1*Gami_1+a2*Gami_2
      ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0

end end end


rescape if CASE == 6 then print('||||| Initialize Jet |||||') remit rquote

      p = P_ref
      u = 0.0
      v = 0.0
      w = 0.0
      r = eps
      r2 = 1.0+0.1*sin(8.0*Pi*x/Ly+0.21*double(id))+0.2*sin(12.0*Pi*y/Ly+0.43*double(id)) -- perturbed as a function of id

      ff[c].r = r
      ff[c].r2 = r2
      ff[c].mu = (r+r2)*u
      ff[c].mv = (r+r2)*v
      ff[c].mw = (r+r2)*w
      var a1 = r*W2/(r*W2+r2*W1)
      var a2 = 1.0-a1
      var gami = a1*Gami_1+a2*Gami_2
      ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0

end end end

    end
end


-- Summation
--__demand(__leaf,__cuda)
task Get_sum(ff: region(ispace(int3d), fc))
where
    reads(ff.{dx,dy,dz,r})
do

    var tot = 0.0
--     __demand(__openmp)
    for c in ff do

       var vol = ff[c].dx*ff[c].dy*ff[c].dz
       tot += tot+vol
      
    end

    return tot

end


-- Copy data
--__demand(__leaf,__cuda)
task CP(ff: region(ispace(int3d), fc))
where
    reads(ff.{r,r2,mu,mv,mw,e}),
    writes(ff.{rc,r2c,muc,mvc,mwc,ec})
do
--     __demand(__openmp)
    for c in ff do
      ff[c].rc = ff[c].r 
      ff[c].r2c = ff[c].r2 
      ff[c].muc = ff[c].mu
      ff[c].mvc = ff[c].mv
      ff[c].mwc = ff[c].mw
      ff[c].ec = ff[c].e
    end
end


function mkRK(ind)
   local RK3

   if ind==1 then print("||||| RK first step |||||")
--   __demand(__leaf,__cuda)
   task RK3(ff: region(ispace(int3d), fc), dt: double)
   where   
       reads(ff.{rc,r2c,muc,mvc,mwc,ec}),
       reads(ff.{r,r2,mu,mv,mw,e,dr,dr2,dmu,dmv,dmw,de}),
       writes(ff.{r,r2,mu,mv,mw,e})                                           
   do                                                                                                               
--        __demand(__openmp)                                                  
       for c in ff do                                                       
          ff[c].r  = [RK[1][1]]*ff[c].rc  + [RK[1][2]]*ff[c].r  + [RK[1][3]]*ff[c].dr*dt 
          ff[c].r2 = [RK[1][1]]*ff[c].r2c + [RK[1][2]]*ff[c].r2 + [RK[1][3]]*ff[c].dr2*dt 
          ff[c].mu = [RK[1][1]]*ff[c].muc + [RK[1][2]]*ff[c].mu + [RK[1][3]]*ff[c].dmu*dt
          ff[c].mv = [RK[1][1]]*ff[c].mvc + [RK[1][2]]*ff[c].mv + [RK[1][3]]*ff[c].dmv*dt
          ff[c].mw = [RK[1][1]]*ff[c].mwc + [RK[1][2]]*ff[c].mw + [RK[1][3]]*ff[c].dmw*dt
          ff[c].e  = [RK[1][1]]*ff[c].ec  + [RK[1][2]]*ff[c].e  + [RK[1][3]]*ff[c].de*dt 
       end                                                                   
   end

   elseif ind==2 then print("||||| RK second step |||||")
--   __demand(__leaf,__cuda)
   task RK3(ff: region(ispace(int3d), fc), dt: double)
   where   
       reads(ff.{rc,r2c,muc,mvc,mwc,ec}),
       reads(ff.{r,r2,mu,mv,mw,e,dr,dr2,dmu,dmv,dmw,de}),
       writes(ff.{r,r2,mu,mv,mw,e})                                           
   do                                                                                                               
--        __demand(__openmp)                                                  
       for c in ff do                                                       
          ff[c].r  = [RK[2][1]]*ff[c].rc  + [RK[2][2]]*ff[c].r  + [RK[2][3]]*ff[c].dr*dt
          ff[c].r2 = [RK[2][1]]*ff[c].r2c + [RK[2][2]]*ff[c].r2 + [RK[2][3]]*ff[c].dr2*dt
          ff[c].mu = [RK[2][1]]*ff[c].muc + [RK[2][2]]*ff[c].mu + [RK[2][3]]*ff[c].dmu*dt
          ff[c].mv = [RK[2][1]]*ff[c].mvc + [RK[2][2]]*ff[c].mv + [RK[2][3]]*ff[c].dmv*dt
          ff[c].mw = [RK[2][1]]*ff[c].mwc + [RK[2][2]]*ff[c].mw + [RK[2][3]]*ff[c].dmw*dt
          ff[c].e  = [RK[2][1]]*ff[c].ec  + [RK[2][2]]*ff[c].e  + [RK[2][3]]*ff[c].de*dt
       end                                                                   
   end

   end

   return RK3
end


function mkBC(dir)

   local BC

   if dir == 1 then
--   __demand(__leaf,__cuda)
   task BC(ff: region(ispace(int3d), fc), xof: int32)
   where
       reads(ff.{r,r2,e,mu,mv,mw,gf,x,y,z,dx}),
       writes(ff.{r,r2,e,mu,mv,mw})
   do
--      __demand(__openmp)
      for c in ff do
  
      var xp = {0,0,0}
      var yp = {0,0,0}
      var zp = {0,0,0}
      var cc = ff[c].gf
      var vs = 1.0
 
rescape if BCx == 0 then remit rquote

      if c.x-xof < bf       then  xp = {nx+xof,0,0}  end
      if c.x-xof > nx+bf-1  then  xp = {-nx+xof,0,0} end

end elseif BCx == 1 then remit rquote

      if c.x-xof == 0     then  xp = {3+xof,0,0}; vs = -1.0 end
      if c.x-xof == 1     then  xp = {1+xof,0,0}; vs = -1.0 end
      if c.x-xof == nx+2  then  xp = {-1+xof,0,0}; vs = -1.0 end
      if c.x-xof == nx+3  then  xp = {-3+xof,0,0}; vs = -1.0 end

end end end

rescape if BCx < 2 then remit rquote

      ff[c].r  = ff[c+xp+yp+zp].r
      ff[c].r2  = ff[c+xp+yp+zp].r2
      ff[c].e  = ff[c+xp+yp+zp].e
      ff[c].mu = vs*ff[c+xp+yp+zp].mu
      ff[c].mv = ff[c+xp+yp+zp].mv
      ff[c].mw = ff[c+xp+yp+zp].mw

end end end
 

rescape if BCx == 2 then remit rquote
-- Dirichlet inflow
      --if c.x < bf       then  xp = {bf-c.x,0,0}  end
      if c.x-xof < bf then

         var y = ff[c].y
         var z = ff[c].z

         -- 2D
         var r = 1.0*exp(-(y*y)/(H_jet*Ly)/(H_jet*Ly))
         var r2 = 1.0-r+eps
         var u = Jet_vel*exp(-(y*y)/(H_jet*Ly)/(H_jet*Ly))
         var v = 0.0
         var w = 0.0

         var p = P_ref
         ff[c].r = r
         ff[c].r2 = r2
         ff[c].mu = (r+r2)*u
         ff[c].mv = 0.0
         ff[c].mw = 0.0
         var a1 = r*W2/(r*W2+r2*W1)
         var a2 = 1.0-a1
         var gami = a1*Gami_1+a2*Gami_2
         ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0

      end

      -- Ghost-cell extlaporation outflow
      if c.x-xof > nx+bf-1  then

         xp = {-(c.x-xof)+(nx+bf-1),0,0}

         ff[c].r  = ff[c+xp+yp+zp].r
         ff[c].r2  = ff[c+xp+yp+zp].r2
         ff[c].e  = ff[c+xp+yp+zp].e
         ff[c].mu = ff[c+xp+yp+zp].mu
         ff[c].mv = ff[c+xp+yp+zp].mv
         ff[c].mw = ff[c+xp+yp+zp].mw  
         
       end

end end end

rescape if BCx == 3 then remit rquote
-- Dirichlet inflow2
      --if c.x < bf       then  xp = {bf-c.x,0,0}  end
      if c.x-xof < bf then

         var x = ff[c].x
         var y = ff[c].y
         var z = ff[c].z

         -- 3D
         var r = 0.5*exp(-(y*y)/0.005)
         var r2 = 1.0-r
         var u = 50.0*exp(-(y*y)/0.005)

         var p = P_ref
         var v = 0.0
         var w = 0.0

         ff[c].r = r
         ff[c].r2 = r2
         ff[c].mu = (r+r2)*u
         ff[c].mv = 0.0
         ff[c].mw = 0.0
         var a1 = r*W2/(r*W2+r2*W1)
         var a2 = 1.0-a1
         var gami = a1*Gami_1+a2*Gami_2
         ff[c].e = p*gami + (r+r2)*(u*u+v*v+w*w)/2.0
      end

      -- Ghost-cell extlaporation outflow
      if c.x-xof > nx+bf-1  then

         xp = {-(c.x-xof)+(nx+bf-1),0,0}

         ff[c].r  = ff[c+xp+yp+zp].r
         ff[c].e  = ff[c+xp+yp+zp].e
         ff[c].mu = ff[c+xp+yp+zp].mu
         ff[c].mv = ff[c+xp+yp+zp].mv
         ff[c].mw = ff[c+xp+yp+zp].mw  
         
       end

end end end

      end
   end

   elseif dir == 2 then
--   __demand(__leaf,__cuda)
   task BC(ff: region(ispace(int3d), fc), xof: int32)
   where
       reads(ff.{r,r2,e,mu,mv,mw,gf}),
       writes(ff.{r,r2,e,mu,mv,mw})
   do
--      __demand(__openmp)
      for c in ff do
  
      var xp = {0,0,0}
      var yp = {0,0,0}
      var zp = {0,0,0}
      var cc = ff[c].gf
      var vs = 1.0
      var ss = 1.0
 
rescape if BCy == 0 then remit rquote

      if c.y < bf       then  yp = {0,ny,0};  end
      if c.y > ny+bf-1  then  yp = {0,-ny,0};  end

end elseif BCy == 1 then remit rquote

      if c.y == 0     then  yp = {0,3,0}; vs = -1.0 end
      if c.y == 1     then  yp = {0,1,0}; vs = -1.0 end
      if c.y == ny+2  then  yp = {0,-1,0}; vs = -1.0 end
      if c.y == ny+3  then  yp = {0,-3,0}; vs = -1.0 end

end elseif BCy == 2 then remit rquote

-- Ghost
      if c.y < bf       then  yp = {0,bf-c.y,0}  end
      if c.y > ny+bf-1  then  yp = {0,-c.y+(ny+bf-1),0} end

end end end

      ff[c].r  = ff[c+xp+yp+zp].r
      ff[c].r2  = ff[c+xp+yp+zp].r2
      ff[c].e  = ff[c+xp+yp+zp].e
      ff[c].mu = ss*ff[c+xp+yp+zp].mu
      ff[c].mv = vs*ff[c+xp+yp+zp].mv
      ff[c].mw = ss*ff[c+xp+yp+zp].mw
  
      end
   end

   end
   return BC

end


--__demand(__leaf,__cuda)
task CtoP(ff: region(ispace(int3d), fc))
where
    reads(ff.{r,r2,mu,mv,mw,e}),
    writes(ff.{u,v,w,p})
do

    var r: double
    var r2: double
    var a1: double
    var a2: double
    var gami: double

--     __demand(__openmp)
    for c in ff do

       ff[c].u = ff[c].mu/(ff[c].r+ff[c].r2)
       ff[c].v = ff[c].mv/(ff[c].r+ff[c].r2)
       ff[c].w = ff[c].mw/(ff[c].r+ff[c].r2)

       a1 = ff[c].r*W2/(ff[c].r*W2+ff[c].r2*W1)
       a2 = 1.0-a1
       gami = a1*Gami_1+a2*Gami_2

       ff[c].p = 1.0/gami*(ff[c].e - (ff[c].mu*ff[c].mu + ff[c].mv*ff[c].mv + ff[c].mw*ff[c].mw)/2.0/(ff[c].r+ff[c].r2))

    end

end


--WENO to obtain y(i+1/2)
__demand(__inline)
task WENO3(ym1: double, y: double, yp1: double)
 
   var aux1 = ym1 - y;
   var aux2 = y - yp1;
   var s1 = aux1*aux1;
   var s2 = aux2*aux2;
   
   var tau = fabs(s1-s2);
   var chi1 = tau/(s1 + weps);
   var chi2 = tau/(s2 + weps);
   
   var a1 = 1.0/3.0*(1.0+chi1);
   var a2 = 2.0/3.0*(1.0+chi2);
   
   var Var1 = -1.0/2.0*ym1 + 3.0/2.0*y -y;
   var Var2 = 1.0/2.0*y + 1.0/2.0*yp1 -y;
   var a = 1.0/(a1 + a2);
   var w1 = a1*a;
   var w2 = a2*a;

   return y + w1*Var1 + w2*Var2

end


-- Euler flux
--local mkEuler = terralib.memoize(function(dir) -- not terra imple for now
function mkEuler(dir)

   local Euler

   if dir == 1 then print('||||| x-Euler flux |||||')
--   __demand(__leaf,__cuda)
   task Euler(ff: region(ispace(int3d), fc) ) -- pi, pb
   where
       reads(ff.{dx,dy,dz,r,r2,u,v,w,p}), -- primitive
       reads writes(ff.{dr,dr2,dmu,dmv,dmw,de})
   do
  
       var lo = ff.bounds.lo + {2,2,2}
       var hi = ff.bounds.hi - {2,2,2}
       var rect2 = rect3d {lo,hi}

       var rLm: double; var rLp: double
       var rRm: double; var rRp: double
       var r2Lm: double; var r2Lp: double
       var r2Rm: double; var r2Rp: double
       var uLm: double; var uLp: double
       var uRm: double; var uRp: double
       var vLm: double; var vLp: double
       var vRm: double; var vRp: double
       var wLm: double; var wLp: double
       var wRm: double; var wRp: double
       var pLm: double; var pLp: double
       var pRm: double; var pRp: double

       var v_L: double; var v_R: double; var v_a: double
       var E_L: double; var E_R: double
       var H_L: double; var H_R: double
       var c_L: double; var c_R: double; var c_a: double
       var s_L: double; var s_R: double; var s_S: double; var s_M: double; var s_P: double
       var xi_L: double; var xi_R: double; var xi_M: double; var xi_P: double

       var drm: double;  var drp: double
       var dr2m: double; var dr2p: double
       var dmum: double; var dmup: double
       var dmvm: double; var dmvp: double
       var dmwm: double; var dmwp: double
       var dem: double;  var dep: double

       var a1: double; var a2: double
       var gami: double; var gam: double

       var pmm = {-2,0,0}
       var pm  = {-1,0,0}
       var pp  = {1,0,0}
       var ppp = {2,0,0}

--        __demand(__openmp)
---       for c in ff2 do
       for c in rect2 do
   
          rLm = WENO3(ff[c+pmm].r,  ff[c+pm].r,  ff[c].r)
          rLp = WENO3(ff[c+pp].r,   ff[c].r,     ff[c+pm].r)
          r2Lm = WENO3(ff[c+pmm].r2,  ff[c+pm].r2,  ff[c].r2)
          r2Lp = WENO3(ff[c+pp].r2,   ff[c].r2,     ff[c+pm].r2)
          uLm = WENO3(ff[c+pmm].u, ff[c+pm].u, ff[c].u)
          uLp = WENO3(ff[c+pp].u,  ff[c].u,    ff[c+pm].u)
          vLm = WENO3(ff[c+pmm].v, ff[c+pm].v, ff[c].v)
          vLp = WENO3(ff[c+pp].v,  ff[c].v,    ff[c+pm].v)
          wLm = WENO3(ff[c+pmm].w, ff[c+pm].w, ff[c].w)
          wLp = WENO3(ff[c+pp].w,  ff[c].w,    ff[c+pm].w)
          pLm = WENO3(ff[c+pmm].p,  ff[c+pm].p,  ff[c].p)
          pLp = WENO3(ff[c+pp].p,   ff[c].p,     ff[c+pm].p)
      
          rRm = WENO3(ff[c+pm].r,   ff[c].r,     ff[c+pp].r)
          rRp = WENO3(ff[c+ppp].r,  ff[c+pp].r,  ff[c].r)
          r2Rm = WENO3(ff[c+pm].r2,   ff[c].r2,     ff[c+pp].r2)
          r2Rp = WENO3(ff[c+ppp].r2,  ff[c+pp].r2,  ff[c].r2)
          uRm = WENO3(ff[c+pm].u,  ff[c].u,    ff[c+pp].u)
          uRp = WENO3(ff[c+ppp].u, ff[c+pp].u, ff[c].u)
          vRm = WENO3(ff[c+pm].v,  ff[c].v,    ff[c+pp].v)
          vRp = WENO3(ff[c+ppp].v, ff[c+pp].v, ff[c].v)
          wRm = WENO3(ff[c+pm].w,  ff[c].w,    ff[c+pp].w)
          wRp = WENO3(ff[c+ppp].w, ff[c+pp].w, ff[c].w)
          pRm = WENO3(ff[c+pm].p,   ff[c].p,     ff[c+pp].p)
          pRp = WENO3(ff[c+ppp].p,  ff[c+pp].p,  ff[c].p)
 
       -- Obtain the incoming flux at the left cell boundary
          v_L = uLm -- x-minus
          v_R = uLp -- x-plus
          v_a = (v_L + v_R)/2.0

          a1 = rLm*W2/(rLm*W2+r2Lm*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0

          E_L = gami*pLm + (rLm+r2Lm)*(uLm*uLm+vLm*vLm+wLm*wLm)/2.0
          c_L = sqrt(gam*pLm/(rLm+r2Lm))

          a1 = rLp*W2/(rLp*W2+r2Lp*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0

          E_R = gami*pLp + (rLp+r2Lp)*(uLp*uLp+vLp*vLp+wLp*wLp)/2.0
          c_R = sqrt(gam*pLp/(rLp+r2Lp))

          c_a = (c_L + c_R)/2.0 -- for now arithmetic average
          s_L = min(v_L - c_L, v_a - c_a)
          s_R = max(v_R + c_R, v_a + c_a)
          s_S = (pLp - pLm + (rLm+r2Lm)*v_L*(s_L - v_L) - (rLp+r2Lp)*v_R*(s_R - v_R))/((rLm+r2Lm)*(s_L - v_L) - ((rLp+r2Lp)*(s_R - v_R)))
          s_M = min(0.0, s_L)
          s_P = max(0.0, s_R)
          xi_L = (s_L - v_L)/(s_L - s_S)
          xi_R = (s_R - v_R)/(s_R - s_S)
          xi_M = (0.5 + 0.5*copysign(1.0,s_S))
          xi_P = (0.5 - 0.5*copysign(1.0,s_S))

          drm  = xi_M*rLm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*rLp*(v_R + s_P*(xi_R - 1.0))
          dr2m = xi_M*r2Lm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*r2Lp*(v_R + s_P*(xi_R - 1.0))

          dmum = xi_M*((rLm+r2Lm)*(v_L*v_L + s_M*(xi_L*s_S - uLm)) + pLm)
               + xi_P*((rLp+r2Lp)*(v_R*v_R + s_P*(xi_R*s_S - uLp)) + pLp)
          dmvm = xi_M*((rLm+r2Lm)*(v_L*vLm + s_M*(xi_L*vLm - vLm)))
               + xi_P*((rLp+r2Lp)*(v_R*vLp + s_P*(xi_R*vLp - vLp)))
          dmwm = xi_M*((rLm+r2Lm)*(v_L*wLm + s_M*(xi_L*wLm - wLm)))
               + xi_P*((rLp+r2Lp)*(v_R*wLp + s_P*(xi_R*wLp - wLp)))
          dem  = xi_M*(v_L*(E_L + pLm) + s_M*(xi_L*(E_L + (s_S-v_L)*((rLm+r2Lm)*s_S + pLm/(s_L-v_L))) - E_L))
               + xi_P*(v_R*(E_R + pLp) + s_P*(xi_R*(E_R + (s_S-v_R)*((rLp+r2Lp)*s_S + pLp/(s_R-v_R))) - E_R))

       -- Obtain the incoming flux at the right cell boundary (L/R flipped for symmetry)
          v_L = uRm -- x-minus
          v_R = uRp -- x-plus
          v_a = (v_L + v_R)/2.0

          a1 = rRm*W2/(rRm*W2+r2Rm*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0
          E_L = gami*pRm + (rRm+r2Rm)*(uRm*uRm+vRm*vRm+wRm*wRm)/2.0
          c_L = sqrt(gam*pRm/(rRm+r2Rm))

--          H_L = pRp/(Gam-1.0) + rRp*v_L*uRp/2.0 + pRp
--          H_R = pRm/(Gam-1.0) + rRm*v_R*uRm/2.0 + pRm

          a1 = rRp*W2/(rRp*W2+r2Rp*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0
          E_R = gami*pRp + (rRp+r2Rp)*(uRp*uRp+vRp*vRp+wRp*wRp)/2.0
          c_R = sqrt(gam*pRp/(rRp+r2Rp))

          c_a = (c_L + c_R)/2.0 -- for now arithmetic average
          s_L = min(v_L - c_L, v_a - c_a)
          s_R = max(v_R + c_R, v_a + c_a)
          s_S = (pRp - pRm + (rRm+r2Rm)*v_L*(s_L - v_L) - (rRp+r2Rp)*v_R*(s_R - v_R))/((rRm+r2Rm)*(s_L - v_L) - ((rRp+r2Rp)*(s_R - v_R)))
          s_M = min(0.0, s_L)
          s_P = max(0.0, s_R)
          xi_L = (s_L - v_L)/(s_L - s_S)
          xi_R = (s_R - v_R)/(s_R - s_S)
          xi_M = (0.5 + 0.5*copysign(1.0,s_S))
          xi_P = (0.5 - 0.5*copysign(1.0,s_S))

          drp  = xi_M*rRm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*rRp*(v_R + s_P*(xi_R - 1.0))
          dr2p = xi_M*r2Rm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*r2Rp*(v_R + s_P*(xi_R - 1.0))

          dmup = xi_M*((rRm+r2Rm)*(v_L*v_L + s_M*(xi_L*s_S - uRm)) + pRm)
               + xi_P*((rRp+r2Rp)*(v_R*v_R + s_P*(xi_R*s_S - uRp)) + pRp)
          dmvp = xi_M*((rRm+r2Rm)*(v_L*vRm + s_M*(xi_L*vRm - vRm)))
               + xi_P*((rRp+r2Rp)*(v_R*vRp + s_P*(xi_R*vRp - vRp)))
          dmwp = xi_M*((rRm+r2Rm)*(v_L*wRm + s_M*(xi_L*wRm - wRm)))
               + xi_P*((rRp+r2Rp)*(v_R*wRp + s_P*(xi_R*wRp - wRp)))
          dep  = xi_M*(v_L*(E_L + pRm) + s_M*(xi_L*(E_L + (s_S-v_L)*((rRm+r2Rm)*s_S + pRm/(s_L-v_L))) - E_L))
               + xi_P*(v_R*(E_R + pRp) + s_P*(xi_R*(E_R + (s_S-v_R)*((rRp+r2Rp)*s_S + pRp/(s_R-v_R))) - E_R))

          ff[c].dr  = ff[c].dr  + (drm - drp)/ff[c].dx
          ff[c].dr2 = ff[c].dr2 + (dr2m - dr2p)/ff[c].dx
          ff[c].dmu = ff[c].dmu + (dmum - dmup)/ff[c].dx
          ff[c].dmv = ff[c].dmv + (dmvm - dmvp)/ff[c].dx
          ff[c].dmw = ff[c].dmw + (dmwm - dmwp)/ff[c].dx
          ff[c].de  = ff[c].de  + (dem - dep)/ff[c].dx

       end
   end

   elseif dir == 2 then print('||||| y-Euler flux |||||')
--   __demand(__leaf,__cuda)
   task Euler(ff: region(ispace(int3d), fc) ) -- pi, pb
   where
       reads(ff.{dx,dy,dz,r,r2,u,v,w,p}), -- primitive
       reads writes(ff.{dr,dr2,dmu,dmv,dmw,de})
   do
  
       var lo = ff.bounds.lo + {2,2,2}
       var hi = ff.bounds.hi - {2,2,2}
       var rect2 = rect3d {lo,hi}

       var rLm: double; var rLp: double
       var rRm: double; var rRp: double
       var r2Lm: double; var r2Lp: double
       var r2Rm: double; var r2Rp: double
       var uLm: double; var uLp: double
       var uRm: double; var uRp: double
       var vLm: double; var vLp: double
       var vRm: double; var vRp: double
       var wLm: double; var wLp: double
       var wRm: double; var wRp: double
       var pLm: double; var pLp: double
       var pRm: double; var pRp: double

       var v_L: double; var v_R: double; var v_a: double
       var E_L: double; var E_R: double
       var H_L: double; var H_R: double
       var c_L: double; var c_R: double; var c_a: double
       var s_L: double; var s_R: double; var s_S: double; var s_M: double; var s_P: double
       var xi_L: double; var xi_R: double; var xi_M: double; var xi_P: double
  
       var drm: double;  var drp: double
       var dr2m: double;  var dr2p: double
       var dmum: double; var dmup: double
       var dmvm: double; var dmvp: double
       var dmwm: double; var dmwp: double
       var dem: double;  var dep: double

       var a1: double; var a2: double
       var gami: double; var gam: double

       var pmm = {0,-2,0}
       var pm  = {0,-1,0}
       var pp  = {0,1,0}
       var ppp = {0,2,0}
    
--        __demand(__openmp)
--       for c in ff2 do
         for c in rect2 do
   
          rLm = WENO3(ff[c+pmm].r, ff[c+pm].r, ff[c].r)
          rLp = WENO3(ff[c+pp].r,  ff[c].r,    ff[c+pm].r)
          r2Lm = WENO3(ff[c+pmm].r2, ff[c+pm].r2, ff[c].r2)
          r2Lp = WENO3(ff[c+pp].r2,  ff[c].r2,    ff[c+pm].r2)

          uLm = WENO3(ff[c+pmm].u, ff[c+pm].u, ff[c].u)
          uLp = WENO3(ff[c+pp].u,  ff[c].u,    ff[c+pm].u)
          vLm = WENO3(ff[c+pmm].v, ff[c+pm].v, ff[c].v)
          vLp = WENO3(ff[c+pp].v,  ff[c].v,    ff[c+pm].v)
          wLm = WENO3(ff[c+pmm].w, ff[c+pm].w, ff[c].w)
          wLp = WENO3(ff[c+pp].w,  ff[c].w,    ff[c+pm].w)
          pLm = WENO3(ff[c+pmm].p, ff[c+pm].p, ff[c].p)
          pLp = WENO3(ff[c+pp].p,  ff[c].p,    ff[c+pm].p)
      
          rRm = WENO3(ff[c+pm].r,  ff[c].r,    ff[c+pp].r)
          rRp = WENO3(ff[c+ppp].r, ff[c+pp].r, ff[c].r)
          r2Rm = WENO3(ff[c+pm].r2,  ff[c].r2,    ff[c+pp].r2)
          r2Rp = WENO3(ff[c+ppp].r2, ff[c+pp].r2, ff[c].r2)

          uRm = WENO3(ff[c+pm].u,  ff[c].u,    ff[c+pp].u)
          uRp = WENO3(ff[c+ppp].u, ff[c+pp].u, ff[c].u)
          vRm = WENO3(ff[c+pm].v,  ff[c].v,    ff[c+pp].v)
          vRp = WENO3(ff[c+ppp].v, ff[c+pp].v, ff[c].v)
          wRm = WENO3(ff[c+pm].w,  ff[c].w,    ff[c+pp].w)
          wRp = WENO3(ff[c+ppp].w, ff[c+pp].w, ff[c].w)
          pRm = WENO3(ff[c+pm].p,  ff[c].p,    ff[c+pp].p)
          pRp = WENO3(ff[c+ppp].p, ff[c+pp].p, ff[c].p)
 
       -- Obtain the incoming flux at the left cell boundary
          v_L = vLm -- x-minus
          v_R = vLp -- x-plus
          v_a = (v_L + v_R)/2.0

          a1 = rLm*W2/(rLm*W2+r2Lm*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0

          E_L = gami*pLm + (rLm+r2Lm)*(uLm*uLm+vLm*vLm+wLm*wLm)/2.0
          c_L = sqrt(gam*pLm/(rLm+r2Lm))

          a1 = rLp*W2/(rLp*W2+r2Lp*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0

          E_R = gami*pLp + (rLp+r2Lp)*(uLp*uLp+vLp*vLp+wLp*wLp)/2.0
          c_R = sqrt(gam*pLp/(rLp+r2Lp))

          c_a = (c_L + c_R)/2.0 -- for now arithmetic average
          s_L = min(v_L - c_L, v_a - c_a)
          s_R = max(v_R + c_R, v_a + c_a)
          s_S = (pLp - pLm + (rLm+r2Lm)*v_L*(s_L - v_L) - (rLp+r2Lp)*v_R*(s_R - v_R))/((rLm+r2Lm)*(s_L - v_L) - ((rLp+r2Lp)*(s_R - v_R)))
          s_M = min(0.0, s_L)
          s_P = max(0.0, s_R)
          xi_L = (s_L - v_L)/(s_L - s_S)
          xi_R = (s_R - v_R)/(s_R - s_S)
          xi_M = (0.5 + 0.5*copysign(1.0,s_S))
          xi_P = (0.5 - 0.5*copysign(1.0,s_S))

          drm  = xi_M*rLm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*rLp*(v_R + s_P*(xi_R - 1.0))
          dr2m = xi_M*r2Lm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*r2Lp*(v_R + s_P*(xi_R - 1.0))

          dmvm = xi_M*((rLm+r2Lm)*(v_L*v_L + s_M*(xi_L*s_S - vLm)) + pLm)
               + xi_P*((rLp+r2Lp)*(v_R*v_R + s_P*(xi_R*s_S - vLp)) + pLp)
          dmum = xi_M*((rLm+r2Lm)*(v_L*uLm + s_M*(xi_L*uLm - uLm)))
               + xi_P*((rLp+r2Lp)*(v_R*uLp + s_P*(xi_R*uLp - uLp)))
          dmwm = xi_M*((rLm+r2Lm)*(v_L*wLm + s_M*(xi_L*wLm - wLm)))
               + xi_P*((rLp+r2Lp)*(v_R*wLp + s_P*(xi_R*wLp - wLp)))
          dem  = xi_M*(v_L*(E_L + pLm) + s_M*(xi_L*(E_L + (s_S-v_L)*((rLm+r2Lm)*s_S + pLm/(s_L-v_L))) - E_L))
               + xi_P*(v_R*(E_R + pLp) + s_P*(xi_R*(E_R + (s_S-v_R)*((rLp+r2Lp)*s_S + pLp/(s_R-v_R))) - E_R))

       -- Obtain the incoming flux at the right cell boundary (L/R flipped for symmetry)
          v_L = vRm -- x-minus
          v_R = vRp -- x-plus
          v_a = (v_L + v_R)/2.0

          a1 = rRm*W2/(rRm*W2+r2Rm*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0
          E_L = gami*pRm + (rRm+r2Rm)*(uRm*uRm+vRm*vRm+wRm*wRm)/2.0
          c_L = sqrt(gam*pRm/(rRm+r2Rm))

--          H_L = pRp/(Gam-1.0) + rRp*v_L*uRp/2.0 + pRp
--          H_R = pRm/(Gam-1.0) + rRm*v_R*uRm/2.0 + pRm

          a1 = rRp*W2/(rRp*W2+r2Rp*W1)
          a2 = 1.0-a1
          gami = a1*Gami_1+a2*Gami_2
          gam = 1.0/gami + 1.0
          E_R = gami*pRp + (rRp+r2Rp)*(uRp*uRp+vRp*vRp+wRp*wRp)/2.0
          c_R = sqrt(gam*pRp/(rRp+r2Rp))

          c_a = (c_L + c_R)/2.0 -- for now arithmetic average
          s_L = min(v_L - c_L, v_a - c_a)
          s_R = max(v_R + c_R, v_a + c_a)
          s_S = (pRp - pRm + (rRm+r2Rm)*v_L*(s_L - v_L) - (rRp+r2Rp)*v_R*(s_R - v_R))/((rRm+r2Rm)*(s_L - v_L) - ((rRp+r2Rp)*(s_R - v_R)))
          s_M = min(0.0, s_L)
          s_P = max(0.0, s_R)
          xi_L = (s_L - v_L)/(s_L - s_S)
          xi_R = (s_R - v_R)/(s_R - s_S)
          xi_M = (0.5 + 0.5*copysign(1.0,s_S))
          xi_P = (0.5 - 0.5*copysign(1.0,s_S))

          drp  = xi_M*rRm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*rRp*(v_R + s_P*(xi_R - 1.0))
          dr2p = xi_M*r2Rm*(v_L + s_M*(xi_L - 1.0))
               + xi_P*r2Rp*(v_R + s_P*(xi_R - 1.0))

          dmvp = xi_M*((rRm+r2Rm)*(v_L*v_L + s_M*(xi_L*s_S - vRm)) + pRm)
               + xi_P*((rRp+r2Rp)*(v_R*v_R + s_P*(xi_R*s_S - vRp)) + pRp)
          dmup = xi_M*((rRm+r2Rm)*(v_L*uRm + s_M*(xi_L*uRm - uRm)))
               + xi_P*((rRp+r2Rp)*(v_R*uRp + s_P*(xi_R*uRp - uRp)))
          dmwp = xi_M*((rRm+r2Rm)*(v_L*wRm + s_M*(xi_L*wRm - wRm)))
               + xi_P*((rRp+r2Rp)*(v_R*wRp + s_P*(xi_R*wRp - wRp)))
          dep  = xi_M*(v_L*(E_L + pRm) + s_M*(xi_L*(E_L + (s_S-v_L)*((rRm+r2Rm)*s_S + pRm/(s_L-v_L))) - E_L))
               + xi_P*(v_R*(E_R + pRp) + s_P*(xi_R*(E_R + (s_S-v_R)*((rRp+r2Rp)*s_S + pRp/(s_R-v_R))) - E_R))

          ff[c].dr  = ff[c].dr + (drm - drp)/ff[c].dy
          ff[c].dr2 = ff[c].dr2 + (dr2m - dr2p)/ff[c].dy
          ff[c].dmu = ff[c].dmu + (dmum - dmup)/ff[c].dy
          ff[c].dmv = ff[c].dmv + (dmvm - dmvp)/ff[c].dy
          ff[c].dmw = ff[c].dmw + (dmwm - dmwp)/ff[c].dy
          ff[c].de  = ff[c].de + (dem - dep)/ff[c].dy

          end
   end
   end

   return Euler

end


--Pygion
extern task py_sk(f:region(ispace(int3d),fc2), id:int32, step:int32, NE: int32)
where reads (f) end
py_sk:set_task_id(9999)

--__demand(__inner)
task callpy(id: int32, step:int32, f:region(ispace(int3d),fc2))
where
reads(f)
do
    py_sk(f,id,step,NE)
end


__demand(__inner,__replicable)
task work(id: int32, t0: double, s0: int32, t_end: double, f:region(ispace(int3d),fc), flag: int32)
where
reads writes (f)
do

if flag == 0 then

   Grid(f) -- directly iterate over the field (since it is one time)
   Init(f,id) -- directly iterate over the field (since it is one time)
   CtoP(f)

else

   var time = 0.0
   var time_real = time + t0
   var step = 0
   var step_real = step + s0

   while time_real < t_end do

rescape if DEBUG then remit rquote
      if step%conf.debug == 0 then [SIM1.calcsum(conf,step,time_real)]; end -- checking discrete conservation
end end end

      -- Saving previous time step cons. variables for RK
      CP(f)

      -- RK iteration
   rescape for i=1,RKN do remit rquote
    
      -- Compute BC: Fixme can ask for more precise subregion
      rescape for j=1,DIMN do remit rquote
             [mkBC(j)](f,id*(nx+2*bf))
      end end end
    
      -- Convert cons. to prim.
      CtoP(f)
    
      -- Dimensional splitting
      rescape for j=1,DIMN do remit rquote
   
   rescape if ADV then remit rquote
    
      -- Add advection flux to RHS
      [mkEuler(j)](f)

   end end end 
     
      end end end 
   
      -- Advance RK step
      [mkRK(i)](f,conf.dt)
    
      fill(f.{dr,dr2,dmu,dmv,dmw,de},0.0)
   
   end end end

     time += conf.dt
     time_real += conf.dt
     step += 1
     step_real += 1

   end

end

end


__demand(__inner)
task Fill(f: region(ispace(int3d),fc), f_cp: region(ispace(int3d),fc2))
where reads writes (f,f_cp) do

   fill(f.{r,r2,mu,mv,mw,e,dr,dr2,dmu,dmv,dmw,de},0.0)
   fill(f.{u,v,w,p},0.0)
   fill(f.{rc,r2c,muc,mvc,mwc,ec},0.0)
   fill(f.{x,y,z,dx,dy,dz},0.0)
   fill(f.{gf},0)
   fill(f_cp.{r,r2,mu,mv,mw,e},0.0)

end

__demand(__inner)
task main()

   var t_beg = gett()/1000000.0 -- start time
   var t1 = conf.t1
   var t0 = 0.0
   var s0 = 0

   var fi = ispace(int3d,{NE*(nx+2*bf),ny+2*bf,nz+2*bf})
   var Fi = ispace(int3d,{NE,1,1})
   var f  = region(fi,fc)
   var pp = partition(equal,f,Fi)

   var f_cp  = region(fi,fc2)

   Fill(f,f_cp)

   __demand(__index_launch)
   for c in Fi do
      work(int(c.x),t0,s0,t1,pp[c],0)
   end

   if PY then

      copy(f.{r,mu,mv,mw,e},f_cp.{r,mu,mv,mw,e})

      __demand(__index_launch)
      for c in Fi do
         callpy(int(c.x),s0,f_cp)
      end
   end

   for i = 0, conf.step/conf.py do

      t1 = t0 + conf.dt * conf.py

      __demand(__index_launch)
      for c in Fi do
        work(int(c.x),t0,s0,t1,pp[c],1)
      end
   
      -- Calling python!
      if PY then
         copy(f.{r,mu,mv,mw,e},f_cp.{r,mu,mv,mw,e})

          __demand(__index_launch)
         for c in Fi do
            callpy(int(c.x),s0+conf.py,f_cp)
         end
      end

      t0 += conf.dt * conf.py
      s0 += conf.py

   end

   rescape print('end') end
--   format.println("total time ={}", gett()/1000000.0-t_beg) -- end time

end

--regentlib.start(main)
regentlib.saveobj(main,"main2.o","object")
