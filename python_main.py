#!/usr/bin/env python3
# for original run_pass see $LEGION_DIRlanguage/tests/python/run_pass/
# run_pass with sklearn 2
# Kazuki Maeda
# kemaeda@stanford.edu
# 2021


from __future__ import print_function

from collections import OrderedDict
import pygion
from pygion import task, Fspace, R, Region, RW, int32, void, float64
import numpy as np

import time
#from sklearn.datasets import make_blobs
#from sklearn.naive_bayes import GaussianNB
#from sklearn.metrics import brier_score_loss
#from sklearn.calibration import CalibratedClassifierCV
#from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt

# sk-learn classifier example
# https://scikit-learn.org/stable/auto_examples/calibration/plot_calibration.html
#@task(task_id=9999)
#def py_sk():

@task(task_id=9999,
argument_types=[Region,int32,int32,int32],
privileges=[R],
return_type=void,
calling_convention='regent')
def py_sk(fr,ids,step,NE):

#   n_samples = 500000
#   n_bins = 3  # use 3 bins for calibration_curve as we have 3 clusters here
   
   start = time.time()
   
   # Generate 3 blobs with 2 classes where the second blob contains
   # half positive samples and half negative samples. Probability in this
   # blob is therefore 0.5.
#   centers = [(-5, -5), (0, 0), (5, 5)]
#   X, y = make_blobs(n_samples=n_samples, centers=centers, shuffle=False, random_state=42)
#   
#   y[: n_samples // 2] = 0
#   y[n_samples // 2 :] = 1
#   sample_weight = np.random.RandomState(42).rand(y.shape[0])
#   
#   # split train, test for calibration
#   X_train, X_test, y_train, y_test, sw_train, sw_test = train_test_split(
#       X, y, sample_weight, test_size=0.9, random_state=42
#       )
#   
#   # Gaussian Naive-Bayes with no calibration
#   clf = GaussianNB()
#   clf.fit(X_train, y_train)  # GaussianNB itself does not support sample-weights
#   prob_pos_clf = clf.predict_proba(X_test)[:, 1]
#   
#   # Gaussian Naive-Bayes with isotonic calibration
#   clf_isotonic = CalibratedClassifierCV(clf, cv=2, method="isotonic")
#   clf_isotonic.fit(X_train, y_train, sample_weight=sw_train)
#   prob_pos_isotonic = clf_isotonic.predict_proba(X_test)[:, 1]
#   
#   # Gaussian Naive-Bayes with sigmoid calibration
#   clf_sigmoid = CalibratedClassifierCV(clf, cv=2, method="sigmoid")
#   clf_sigmoid.fit(X_train, y_train, sample_weight=sw_train)
#   prob_pos_sigmoid = clf_sigmoid.predict_proba(X_test)[:, 1]
#   
#   print("Brier score losses: (the smaller the better)")
#   
#   clf_score = brier_score_loss(y_test, prob_pos_clf, sample_weight=sw_test)
#   print("No calibration: %1.3f" % clf_score)
#   
#   clf_isotonic_score = brier_score_loss(y_test, prob_pos_isotonic, sample_weight=sw_test)
#   print("With isotonic calibration: %1.3f" % clf_isotonic_score)
#   
#   clf_sigmoid_score = brier_score_loss(y_test, prob_pos_sigmoid, sample_weight=sw_test)
#   print("With sigmoid calibration: %1.3f" % clf_sigmoid_score)
   
   #plt.switch_backend('Agg') 

   arr = fr.r
   #print(arr.shape)

   a_all=arr[:,:,3]
   fall = "all_%03d_%04d.png" % (ids, step)
   if ids == 0:
      plt.imsave(fall,a_all.transpose())
      print(fall)

   end = time.time()
   #print(end-start)
